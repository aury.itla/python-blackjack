﻿import pygame
from pygame.locals import *

pygame.init()


fenetre = pygame.display.set_mode((640, 480))


fond = pygame.image.load("graphics/fondecran.png").convert()
fenetre.blit(fond, (0,0))


pygame.display.flip()

continuer = 1

while continuer:
    for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key ==K_c:
                      import BlackJack
                      import Accueil
                      continuer = 0

                if event.key ==K_n:
                    import Snake
                    import Accueil
                    continuer = 0

                if event.key ==K_g:
                    import Memorie
                    import Accueil
                    continuer = 0

            if event.type == QUIT:
                continuer = 0



pygame.quit()
