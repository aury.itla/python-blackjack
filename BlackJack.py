import pygame
from pygame.locals import *
import subprocess
import random
from fractions import *  #importation des librairie necessaire par la suite


ListeCarte=[['Coeur',2,3,4,5,6,7,8,9,10,'V','Q','K','A'],['Trefle',2,3,4,5,6,7,8,9,10,'V','Q','K','A'],['Carreau',2,3,4,5,6,7,8,9,10,'V','Q','K','A'],['Pique',2,3,4,5,6,7,8,9,10,'V','Q','K','A']] #talon de carte du jeu
CarteMainT2=[] #liste de carte du joueur au tour 2
CarteMainCT2=[] #liste de cartes du croupier au tour 2

x1=200
pdjt2=0
r=280
#############################################################################################################

def tour1 (): #fonction qui fait tirer obligatoirement deux cartes au joueur et au croupier

    x=random.randint(0,len(ListeCarte)-1) # tirage d'une couleur et d'une valeur
    z=ListeCarte[x]
    CouleurC1=z[0]
    ValeurC1=z[random.randint(1,len(z)-1)]

    ListeCarte[x].remove(ValeurC1) #on supprime cette carte de la liste de base


    x=random.randint(0,len(ListeCarte)-1) #tirage d'une couleur et d'une valeur pour le croupier
    z=ListeCarte[x]
    CouleurCroupC1=z[0]
    ValeurCroupC1=z[random.randint(1,len(z)-1)]
    ListeCarte[x].remove(ValeurCroupC1) #on supprime cette carte de la liste de base

    x=random.randint(0,len(ListeCarte)-1) # tirage d'une couleur et d'une valeur
    z=ListeCarte[x]
    CouleurC2=z[0]
    ValeurC2=z[random.randint(1,len(z)-1)]

    ListeCarte[x].remove(ValeurC2) #on supprime cette carte de la liste de base

    x=random.randint(0,len(ListeCarte)-1) #tirage d'une couleur et d'une valeur pour le croupier
    z=ListeCarte[x]
    CouleurCroupC2=z[0]
    ValeurCroupC2=z[random.randint(1,len(z)-1)]
    ListeCarte[x].remove(ValeurCroupC2) #on supprime cette carte de la liste de base



    return CouleurC1, ValeurC1, CouleurC2, ValeurC2, CouleurCroupC1, ValeurCroupC1, CouleurCroupC2, ValeurCroupC2 #on retourne les couleurs et les valeurs des cartes tirees

########################################################################################################################

def tour2 (): #tour qui permet au joueur de tirer un certain nombre de carte a  sa volonter et au croupier de tirer des cartes suivant une IA (voir plus bas)
    global pointdujoueurT1
    boucle=1 #valeur qui permet a  la boucle d'affichage de tourner en continu

    pointdujoueurT22=calculJT2()               #appel de la fonction permettant de calculer les points du joueur au deuxieme tour
    i=0
    menu = pygame.image.load("graphics/board/tapisT2.png").convert()  #affichage de la boite de dialogue en bas
    fenetre.blit(menu, (0,674))
    pygame.display.flip()

    while boucle: #boucle de tirage de crate a  volonte
        for event in pygame.event.get(): #ici les touches a  presser en fonction du choix du joueur
            if event.type == QUIT :
                pygame.quit()
                continuer=0
            if event.type ==KEYDOWN:

                if event.key == K_c :
                    i="c"
                if event.key == K_p :
                    i="p"
                    boucle=0
                    menu = pygame.image.load("graphics/board/tapisvierge.png").convert() #supprimer la boite de dialogue en bas pour la suite
                    fenetre.blit(menu, (0,674))

        if i=='C' or i=='c':
            if pointdujoueurT22<=21:

                x=random.randint(0,len(ListeCarte)-1) #tirage d'une carte
                z=ListeCarte[x]
                CouleurJT2=z[0]
                CarteMainT2.append(CouleurJT2)

                ValeurJT2=z[random.randint(1,len(z)-1)]
                CarteMainT2.append(ValeurJT2)
                ListeCarte[x].remove(ValeurJT2)





                pointdujoueurT22=calculJT2()               #calcul des points du joueur au tour 2


                if pointdujoueurT22<=21 :

                    menu = pygame.image.load("graphics/board/tapisT2.png").convert()
                    fenetre.blit(menu, (0,674))

                pointdujoueurT22=calculJT2()

                if pointdujoueurT22>21:
                    i="p"
                    boucle=0
                    menu = pygame.image.load("graphics/board/tapisvierge.png").convert()
                    fenetre.blit(menu, (0,674))
                pointdujoueurT22=calculJT2()

                global x1
                carteaffichagejoueur,carteaffichagecroup=CarteAffichage() #affichage des cartes a  partir de 3 cartes: 3.4.5...
                e=1
                x1=x1+100
                e=e+1
                Carte3=pygame.image.load(carteaffichagejoueur[e]).convert()
                fenetre.blit(Carte3, (x1,500))
                pygame.display.flip()


            pygame.display.flip()

        if i=='p':
            menu = pygame.image.load("graphics/board/tapisvierge.png").convert()
            fenetre.blit(menu, (0,674))
            boucle=0
        if pointdujoueurT22>21:
            i="p"
            boucle=0
            menu = pygame.image.load("graphics/board/tapisvierge.png").convert()
            fenetre.blit(menu, (0,674))
        i=0
    CarteMainCT2,pointducroupT2=IAT2croupier()    #appel


    return CarteMainT2,pointducroupT2,pointdujoueurT22


########################################################################################################################

def T2croup (): #tirage d'une carte pour le croupier

    x=random.randint(0,len(ListeCarte)-1)
    z=ListeCarte[x]
    CouleurCT2=z[0]
    CarteMainCT2.append(CouleurCT2)

    ValeurCT2=z[random.randint(1,len(z)-1)]
    CarteMainCT2.append(ValeurCT2)
    ListeCarte[x].remove(ValeurCT2)

    return CarteMainCT2

#########################################################################################################################

def IAT2croupier(): #IA du croupier qui dÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©termine s'il pioche une carte ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â  partir de proba dÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©terminee en fonction des points actuels du croupier
    global CarteMainCT2
    ia=1
    pointducroupT1=calculCT1()               #appel
    pointducroupT2=pointducroupT1
    pointdujoueurT22=calculJT2()
    if pointdujoueurT22>21:
        ia=0

    if pointducroupT2<=21 and ia:
        if pointducroupT2<=10 or pointducroupT2<pointdujoueurT22: #100% de chance de tirer une carte

            CarteMainCT2=T2croup()               #appel
        if pointducroupT2==11 and ia:
            p=random.randint(0,100)

            if p<=97 :
                CarteMainCT2=T2croup()               #appel
        if pointducroupT2==12  and ia:
            p=random.randint(0,100)

            if p<=81 :
                CarteMainCT2=T2croup()               #appel
        if pointducroupT2==13 and ia:
            p=random.randint(0,100)

            if p<=73 :
                CarteMainCT2=T2croup()               #appel
        if pointducroupT2==14  and ia:
            p=random.randint(0,100)

            if p<=60 :
                CarteMainCT2=T2croup()               #appel
        if pointducroupT2==15 and ia:
            p=random.randint(0,100)

            if p<=46 :
                CarteMainCT2=T2croup()               #appel
        if pointducroupT2==16 and ia:
            p=random.randint(0,100)

            if p<=25 :
                CarteMainCT2=T2croup()               #appel
        if pointducroupT2==17  and ia:
            p=random.randint(0,100)

            if p<=17 :
                CarteMainCT2=T2croup()               #appel
        if pointducroupT2==18  and ia:
            p=random.randint(0,100)

            if p<=10 :
                CarteMainCT2=T2croup()               #appel
        if pointducroupT2==19 and ia :
            p=random.randint(0,100)

            if p<=7 :
                CarteMainCT2=T2croup()               #appel
        if pointducroupT2==20  and ia:
            p=random.randint(0,100)

            if p<=3 :
                CarteMainCT2=T2croup()               #appel
        pointducroupT2=calculCT2()               #appel

    return CarteMainCT2,pointducroupT2



#########################################################################################################################

def calculJT1():#calcul des points au tour 1
    valcarte1=valretour1
    valcarte2=valretour2
    boucle_as=1

    if valretour1=="V" or valretour1=="Q" or valretour1=="K": #si la carte est un roi, dame ou valet la carte vaut 10
        valcarte1=10

    while valretour1=="A" and boucle_as==1: #demande au joueur combien de point veut il atribuer ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â  son as
        menu = pygame.image.load("graphics/board/tapisaskas.png").convert()
        fenetre.blit(menu, (0,674))
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == QUIT :
                pygame.quit()
                continuer=0
            if event.type ==KEYDOWN:
                if event.key == K_b :
                    valcarte1=1
                    boucle_as=0
                    menu = pygame.image.load("graphics/board/tapisvierge.png").convert()
                    fenetre.blit(menu, (0,674))
                    pygame.display.flip()
                if event.key == K_n :
                    valcarte1=11
                    boucle_as=0
                    menu = pygame.image.load("graphics/board/tapisvierge.png").convert()
                    fenetre.blit(menu, (0,674))
                    pygame.display.flip()
    if valretour2=="V" or valretour2=="Q" or valretour2=="K":
        valcarte2=10
    while valretour2=="A" and boucle_as==1:
        menu = pygame.image.load("graphics/board/tapisaskas.png").convert()
        fenetre.blit(menu, (0,674))
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == QUIT :
                pygame.quit()
                continuer=0
            if event.type ==KEYDOWN:
                if event.key == K_b :
                    valcarte2=1
                    boucle_as=0
                    menu = pygame.image.load("graphics/board/tapisvierge.png").convert()
                    fenetre.blit(menu, (0,674))
                    pygame.display.flip()
                if event.key == K_n :
                    valcarte2=11
                    boucle_as=0
                    menu = pygame.image.load("graphics/board/tapisvierge.png").convert()
                    fenetre.blit(menu, (0,674))
                    pygame.display.flip()

    pointdujoueurT1=int(valcarte1)+int(valcarte2)
    return pointdujoueurT1

#########################################################################################################################

def calculJT2():#calcule les points du joueur au tour 2
    global pdjt2
    pointdujoueurT2=pointdujoueurT1
    jh=len(CarteMainT2)/2
    x=1
    while jh!=0 and pointdujoueurT2<=21:

        if CarteMainT2[x]=="V" or CarteMainT2[x]=="Q" or CarteMainT2[x]=="K":
            CarteMainT2.insert(x,10)
            pointdujoueurT2=pointdujoueurT1+CarteMainT2[x]
            pdjt2=pdjt2+Fraction(CarteMainT2[x],3)
            CarteMainT2.remove(10)
        if CarteMainT2[x]=="A":
            valAs=11
            CarteMainT2.insert(x,valAs)
            pointdujoueurT2=int(pointdujoueurT2)+int(CarteMainT2[x])
            pdjt2=pdjt2+Fraction(CarteMainT2[x],3)
            CarteMainT2.remove(valAs)
        if CarteMainT2[x]!="V" and CarteMainT2[x]!="Q" and CarteMainT2[x]!="K" and CarteMainT2[x]!="A":
            pointdujoueurT2=int(pointdujoueurT2)+int(CarteMainT2[x])
            pdjt2=pdjt2+Fraction(CarteMainT2[x],3)


        jh=jh-1
        x=x+2


    pointdujoueurT22=pdjt2+pointdujoueurT1

    return pointdujoueurT22

#########################################################################################################################

def calculCT1 () : #calcule les points du croupier au tour 1
    valCarte1=valretourCroup1
    valCarte2=valretourCroup2
    if valretourCroup1=="V" or valretourCroup1=="Q" or valretourCroup1=="K":
        valCarte1=10
    if valretourCroup2=="V" or valretourCroup2=="Q" or valretourCroup2=="K":
        valCarte2=10
    if valretourCroup1=="A":
        valCarte1=11
    if valretourCroup2=="A":
        valCarte2=11
        pointducroupT1=valCarte1+valCarte2
        if pointducroupT1>21:
            valCarte2=1
    pointducroupT1=int(valCarte1)+int(valCarte2)
    return pointducroupT1

########################################################################################################################

def calculCT2 () : #calcule les points du croupier au tour 2
    pointducroupT1=calculCT1()               #appel
    pointducroupT2=pointducroupT1
    jh=len(CarteMainCT2)/2

    while jh!=0 and pointducroupT2<=21:
        x=1
        if CarteMainCT2[x]=="V" or CarteMainCT2[x]=="Q" or CarteMainCT2[x]=="K":
            CarteMainCT2.insert(x,10)
            pointducroupT2=pointducroupT1+CarteMainCT2[x]
            CarteMainCT2.remove(10)

        if CarteMainCT2[x]=="A":
            valAs=11
            CarteMainCT2.insert(x,valAs)
            pointducroupT2=int(pointducroupT2)+int(CarteMainCT2[x])
            CarteMainCT2.remove(valAs)
            if pointducroupT2>21 :
                valAs=1
                CarteMainCT2.insert(x,valAs)
                pointducroupT2=int(pointducroupT2)+int(CarteMainCT2[x])
                CarteMainCT2.remove(valAs)

        if CarteMainCT2[x]!="V" and CarteMainCT2[x]!="Q" and CarteMainCT2[x]!="K" and CarteMainCT2[x]!="A":
            pointducroupT2=int(pointducroupT2)+int(CarteMainCT2[x])

        jh=jh-1
        x=x+2
    return pointducroupT2

########################################################################################################################

def CarteAffichage (): #algorithme qui permet de sortir un nom de carte avec l'extension ".png"
    global LongueurT2
    global CarteMainT2
    global LongueurCT2
    Carte1='graphics/cards/'+coulretour1+str(valretour1)+'.png'
    Carte2='graphics/cards/'+coulretour2+str(valretour2)+'.png'
    carteaffichagejoueur=[Carte1,Carte2]

    CarteC1='graphics/cards/'+coulretourCroup1+str(valretourCroup1)+'.png'
    CarteC2='graphics/cards/'+coulretourCroup2+str(valretourCroup2)+'.png'
    carteaffichagecroup=[CarteC1,CarteC2]
    CarteMainT2bis=CarteMainT2
    CarteMainT2=CarteMainT2

    LongueurT2=len(CarteMainT2bis)/2

    jh=len(CarteMainT2bis)/2 #nombre de tour de while pour afficher les cartes joueur
    while jh!=0 :
        CarteX='graphics/cards/'+CarteMainT2bis[0]+str(CarteMainT2bis[1])+'.png'

        carteaffichagejoueur.append(CarteX)
        jh=jh-1
        CarteMainT2bis.remove(CarteMainT2bis[0])
        CarteMainT2bis.remove(CarteMainT2bis[0])



    LongueurCT2=(len(CarteMainCT2))/2
    jh2=(len(CarteMainCT2))/2 #nombre de tour de while pour afficher les cartes croupier
    while LongueurCT2>0 :
        CarteX1='graphics/cards/'+CarteMainCT2[0]+str(CarteMainCT2[1])+'.png'
        CarteMainCT2.remove(CarteMainCT2[0])
        CarteMainCT2.remove(CarteMainCT2[0])
        carteaffichagecroup.append(CarteX1)

        LongueurCT2=LongueurCT2-1

    return carteaffichagejoueur, carteaffichagecroup


#######################################################################################################################

pygame.init()

#Ouverture de la fenetre Pygame
fenetre = pygame.display.set_mode((1200,800))

#Chargement et collage du fond
fond = pygame.image.load("graphics/board/.tapisvierge.png").convert()
fenetre.blit(fond, (0,0))

menu = pygame.image.load("graphics/board/tapisStart.png").convert()
fenetre.blit(menu, (0,674))

winstreak = pygame.image.load("graphics/board/winstreak.png").convert()
fenetre.blit(winstreak, (63,302))

DosDeCarte= pygame.image.load("graphics/board/Dosdecarte.png").convert_alpha()
fenetre.blit(DosDeCarte, (1000,250))

#Rafraichissement de l'ecran
pygame.display.flip()

pygame.mixer.music.load("music/blackjack.mp3")
pygame.mixer.music.play(-1)

#BOUCLE INFINIE
continuer = 1

while continuer:

    for event in pygame.event.get():

        if event.type == QUIT :
            pygame.quit()
            continuer=0

        if event.type == KEYDOWN:
            if event.key== K_s:
                if event.type == QUIT :
                    pygame.quit()
                    continuer=0


                coulretour1,valretour1,coulretour2,valretour2,coulretourCroup1,valretourCroup1,coulretourCroup2,valretourCroup2=tour1()
                carteaffichagejoueur,carteaffichagecroup=CarteAffichage()

                Carte1=pygame.image.load(carteaffichagejoueur[0]).convert()
                fenetre.blit(Carte1, (100,500))
                pygame.display.flip()
                Carte2=pygame.image.load(carteaffichagejoueur[1]).convert()
                fenetre.blit(Carte2, (200,500))
                pygame.display.flip()

                CarteC1=pygame.image.load(carteaffichagecroup[0]).convert()
                fenetre.blit(CarteC1, (100,25))
                pygame.display.flip()
                CarteC2=pygame.image.load(carteaffichagecroup[1]).convert()
                fenetre.blit(CarteC2, (200,25))
                pygame.display.flip()


                pointdujoueurT1=calculJT1()               #appel de la fonction permettant de calculer les points du joueur au premier tour
                menu = pygame.image.load("graphics/board/tapisContinue.png").convert()
                fenetre.blit(menu, (0,674))
                pygame.display.flip()


        if event.type == KEYDOWN:
            if event.key ==K_d:
                if event.type == QUIT :
                    pygame.quit()
                    continuer=0


                CarteMainT2,pointducroupT2,pointdujoueurT22=tour2()

                carteaffichagejoueur,carteaffichagecroup=CarteAffichage()

                if len(carteaffichagecroup)>=3:
                    CarteC3=pygame.image.load(carteaffichagecroup[2]).convert()
                    fenetre.blit(CarteC3, (300,25))
                    pygame.display.flip()
                if len(carteaffichagecroup)>=4:
                    CarteC4=pygame.image.load(carteaffichagecroup[3]).convert()
                    fenetre.blit(CarteC4, (400,25))
                    pygame.display.flip()
                if len(carteaffichagecroup)>=5:
                    CarteC5=pygame.image.load(carteaffichagecroup[4]).convert()
                    fenetre.blit(CarteC5, (500,25))
                    pygame.display.flip()

                if pointducroupT2<pointdujoueurT22 and pointdujoueurT22<=21:
                    menu = pygame.image.load("graphics/board/tapiswin.png").convert()
                    fenetre.blit(menu, (0,674))
                    winstreak= pygame.image.load("graphics/board/winstreakbarre.png")
                    fenetre.blit(winstreak, (r, 314))
                    r=r+7

                if pointducroupT2>21 and pointdujoueurT22<=21:
                    menu = pygame.image.load("graphics/board/tapiswin.png").convert()
                    fenetre.blit(menu, (0,674))
                    winstreak= pygame.image.load("graphics/board/winstreakbarre.png")
                    fenetre.blit(winstreak, (r, 314))
                    r=r+7

                if pointducroupT2>=pointdujoueurT22 and pointducroupT2<=21:


                    menu = pygame.image.load("graphics/board/tapislose.png").convert()
                    fenetre.blit(menu, (0,674))


                if 21<pointdujoueurT22:


                    menu = pygame.image.load("graphics/board/tapislose.png").convert()
                    fenetre.blit(menu, (0,674))

                pygame.display.flip()

                pygame.time.delay(1000)
                menu = pygame.image.load("graphics/board/tapisfin5.png").convert()
                fenetre.blit(menu, (785,712))
                pygame.time.delay(1000)
                pygame.display.flip()
                menu = pygame.image.load("graphics/board/tapisfin4.png").convert()
                fenetre.blit(menu, (785,712))
                pygame.time.delay(1000)
                pygame.display.flip()
                menu = pygame.image.load("graphics/board/tapisfin3.png").convert()
                fenetre.blit(menu, (785,712))
                pygame.time.delay(1000)
                pygame.display.flip()
                menu = pygame.image.load("graphics/board/tapisfin2.png").convert()
                fenetre.blit(menu, (785,712))
                pygame.time.delay(1000)
                pygame.display.flip()
                menu = pygame.image.load("graphics/board/tapisfin1.png").convert()
                fenetre.blit(menu, (785,712))
                pygame.time.delay(1000)
                pygame.display.flip()
                pygame.time.delay(1000)

                fond = pygame.image.load("graphics/board/.tapisvierge2.png").convert_alpha()
                fenetre.blit(fond, (0,0))

                if pointducroupT2>=pointdujoueurT22 and pointducroupT2<=21:

                    fond = pygame.image.load("graphics/board/.tapisvierge.png").convert_alpha()
                    fenetre.blit(fond, (0,0))
                    winstreak = pygame.image.load("graphics/board/winstreak.png").convert()
                    fenetre.blit(winstreak, (63,302))
                if 21<pointdujoueurT22:

                    fond = pygame.image.load("graphics/board/.tapisvierge.png").convert_alpha()
                    fenetre.blit(fond, (0,0))
                    winstreak = pygame.image.load("graphics/board/winstreak.png").convert()
                    fenetre.blit(winstreak, (63,302))

                menu = pygame.image.load("graphics/board/tapisStart.png").convert()
                fenetre.blit(menu, (0,674))


                DosDeCarte= pygame.image.load("graphics/board/Dosdecarte.png").convert_alpha()
                fenetre.blit(DosDeCarte, (1000,250))
                pygame.display.flip()

                ListeCarte=[['Coeur',2,3,4,5,6,7,8,9,10,'V','Q','K'],['Trefle',2,3,4,5,6,7,8,9,10,'V','Q','K'],['Carreau',2,3,4,5,6,7,8,9,10,'V','Q','K'],['pique',2,3,4,5,6,7,8,9,10,'V','Q','K']]
                CarteMainCT2=[]

                x1=200
                pdjt2=0



