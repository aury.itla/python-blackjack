﻿import pygame
from pygame.locals import *
from random import randint
#################################################### variables###########################################################################
X=16
Y=16

TAILLE=32

BAS= (0, +1)

DROIT= (+1,0)

HAUT=(0, -1)

GAUCHE=(-1, 0)

FPS_CAP=10

####################################################fonctions#############################################################################

def main ():#principale,mise en place des images et des textes
    global score, font
    pygame.init()
    fenetre = pygame.display.set_mode ((800, 800))

    fond = pygame.image.load("graphics/snake/fond.jpg").convert()

    fenetre.blit(fond, (0,0))

    font=pygame.font.Font(None, 24)

    fond=pygame.transform.scale(fond, (800, 800))

    go=pygame.image.load("graphics/snake/game over.jpg").convert()

    go=pygame.transform.scale(go, (800, 800))

    tps=pygame.time.Clock() #fps du jeu

    mainloop(fenetre, tps, fond, go)




def mainloop (fenetre, tps, fond, go):#boucle principale qui accione le jeu et les commandes
    global orientation, reset
    running=1
    reset()
    while running:
        tps.tick(FPS_CAP)
        for event in pygame.event.get():
            if event.type == QUIT:
                running =0

            if event.type ==KEYDOWN:
                if event.key== K_RETURN:
                    reset()

                if event.key ==K_UP:
                    if orientation!=BAS:
                        orientation = HAUT

                if event.key ==K_DOWN:
                    if orientation!=HAUT:
                        orientation = BAS

                if event.key ==K_RIGHT:
                    if orientation!=GAUCHE:
                        orientation = DROIT


                if event.key ==K_LEFT:
                    if orientation!=DROIT:
                        orientation = GAUCHE
        if not fin:
            MAJ(fenetre)
        rendu(fenetre, fond, pomme,go)
        pygame.display.update()


    pygame.quit()




def MAJ (fenetre): #mise a jour du jeu , colisions et création du serpent
    global pomme, partie_serpent, taille_serpent, orientation, fin, score
    x, y = partie_serpent [-1]
    x += orientation [0]
    y += orientation [1]
    nouveau_corps = (x, y)


    if nouveau_corps in partie_serpent:
        fin=1

    if not 3 <=x <=21:
        fin=1

    if not 2 <=y <=16:
        fin=1

        fin=1

    if nouveau_corps == pomme:
        taille_serpent +=1
        score= score+1
        pomme_suivant()



    partie_serpent.append(nouveau_corps)

    if not nouveau_corps in partie_serpent:
        fin=1

    if not fin:
        if len(partie_serpent)>taille_serpent:
            del partie_serpent[0]




def rendu(fenetre, fond,pomme,go):
    global partie_serpent, taille_serpent, orientation, fin, font
    fenetre.blit(fond, (0,0))

    font = pygame.font.SysFont(None, 50)
    score_affiche = font.render(str(score), 1, (0,0,0))#problème pour score et text

    font2 = pygame.font.SysFont(None, 32)
    fenetre.blit(score_affiche, (500, 565))


    for part in partie_serpent:
       cellule_dessin(fenetre, part, ((100,255,200)))#cellule partie serpent
       if fin:
        fenetre.fill((0,0,0))
        fenetre.blit(go, (0,0))




    cellule_dessin(fenetre, pomme, ((255,0,0)))
    if fin:
        fenetre.fill((0,0,0))
        fenetre.blit(go, (0,0))
        fenetre.blit(score_affiche, (600, 700))
        score_affiche2 = font2.render("Score:", 1, (0,0,0))#problème pour score et text
        fenetre.blit(score_affiche2, (500, 710))





def cellule_dessin(fenetre,cell,color):#création d'un rectangle afin de diminuer le code
    global x,y
    pygame.draw.rect(fenetre, color, (cell[0]*TAILLE, cell[1]*TAILLE, TAILLE, TAILLE))


def pomme_suivant ():
    global pomme#variable globale
    x= randint (3,21)
    y= randint (2,16)
    pomme= (x, y)



def reset():
    global partie_serpent, taille_serpent, orientation, fin, pomme, score#reset le jeu
    pomme_suivant()
    partie_serpent = [(5,5)]
    score = 0
    taille_serpent =6
    orientation=BAS
    fin=0




main()
